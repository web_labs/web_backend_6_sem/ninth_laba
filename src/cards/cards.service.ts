import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { CardsRepository } from './cards.repository';
import { Card } from './entities/card.entity';

@Injectable()
export class CardsService extends TypeOrmCrudService<Card> {
    constructor(
        @InjectRepository(CardsRepository) repo: CardsRepository
    ) {
        super(repo);
    }
}
