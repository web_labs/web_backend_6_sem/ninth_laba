import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CreateColumnDto {
  @ApiProperty({ example: 'name', description: 'name', type: String })
  @IsString()
  name: string;
}
